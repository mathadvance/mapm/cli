# mapm-cli

This is the command-line interface for mapm. For the graphical interface, see [mapm-gui](https://gitlab.com/mathadvance/mapm/gui).

mapm is a problem-management software that allows you to quickly iterate through drafts of exams. By modularizing your problems into their own files, you can easily mix and match problems and compile exams and solution manuals.

By using built-in TeX macros and a sane implementation of the incredibly human-readable YAML configuration language, you can spend less time learning how to organize your exam creation process and more time on the exam itself.

Read the usage instructions at [mapm.mathadvance.org/](https://mapm.mathadvance.org).

## Is this for me?

mapm is meant for you if you are an exam-creator or contest writer whose template has a **fixed** number of problems. Thus, contests like the AMCs can easily be typeset with mapm, while exams like the SAT or school tests with a variable number of questions do not have the same level of support.

## Install

If you have Rust on your system, run `cargo install mapm-cli` to get the latest and greatest from cargo. If you do not, binaries will be released for every major version.

You can also run `git clone` and `cargo build` to manually install from source.

## Philosophy

I am of the belief that you should modify the source code to fit any edge-case needs, rather than the code providing a bunch of complex hooks that would be more difficult to use than simpler source code.

Therefore, users are encouraged to keep their own builds of any `mapm` binaries if inclined to, and to propose any particularly well-designed solutions to upstream.
