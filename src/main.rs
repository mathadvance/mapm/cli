pub mod cli;
pub mod commands;
pub mod utils;

#[quit::main]
fn main() {
    cli::parse_args();
}
