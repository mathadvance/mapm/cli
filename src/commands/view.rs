use crate::utils::cli_display::{display, problems_to_string};
use crate::utils::profile::get_profile;
use crate::utils::views::{parse_views, problem_names_to_problems};

pub fn view(problems: Vec<String>, hide: Option<Vec<String>>, show: Option<Vec<String>>) {
    let views = parse_views(hide, show);
    let problem_display = problems_to_string(&problem_names_to_problems(
        &get_profile().unwrap(),
        &problems,
        views,
    ));
    display(&problem_display);
}
