use crate::utils::dirs;
use std::fs;

pub fn rename(target: String, destination: String) {
    fs::rename(
        dirs::problem_dir().unwrap().join(format!("{}.yml", target)),
        dirs::problem_dir()
            .unwrap()
            .join(format!("{}.yml", destination)),
    )
    .expect("Failed to rename problem");
}
