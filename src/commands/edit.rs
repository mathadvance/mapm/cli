use crate::utils::dirs::problem_dir;

pub fn edit(problems: Vec<String>) {
    let problem_args: Vec<String> = problems
        .into_iter()
        .map(|s| {
            String::from(
                problem_dir()
                    .unwrap()
                    .join(&[s.as_str(), ".yml"].concat())
                    .to_str()
                    .unwrap(),
            )
        })
        .collect();
    let editor: String;
    match std::env::var("EDITOR") {
        Ok(var) => {
            editor = var;
        }
        Err(_) => {
            if cfg!(windows) {
                editor = String::from("notepad");
            } else {
                editor = String::from("nano");
            }
        }
    }
    std::process::Command::new(&editor)
        .args(problem_args)
        .status()
        .unwrap_or_else(|_| panic!("{}", ["Failed to start `", &editor, "`"].concat()));
}
