use crate::utils::dirs::template_dir;
use mapm::contest::Contest;
use mapm::problem::{Problem, Solutions, Vars};
use mapm::template::fetch_template_config;
use std::collections::HashMap;

// Generates the contest yml for preview

pub fn preview_yml(problem_name: &str) -> String {
    format!(
        "template: preview\nvars: {{}}\nproblems:\n- {}",
        problem_name
    )
}

// Generates the Contest for preview-all

pub fn preview_all_contest(problems: &[(String, Vars, Option<Solutions>)]) -> Contest {
    let problems = problems
        .iter()
        .map(|problem| match &problem.2 {
            Some(solutions) => Problem {
                name: problem.0.clone(),
                vars: problem.1.clone(),
                solutions: solutions.to_vec(),
            },
            None => Problem {
                name: problem.0.clone(),
                vars: problem.1.clone(),
                solutions: Vec::new(),
            },
        })
        .collect();
    let template = fetch_template_config("preview-all", &template_dir()).unwrap();
    Contest {
        problems,
        problem_count: None,
        vars: HashMap::new(),
        template,
    }
}
