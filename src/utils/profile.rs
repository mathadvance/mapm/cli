//! Setting mapm profile (and other stuff that may be added)

use std::fs;
use std::io;
use std::path::Path;

use colour::*;

pub fn get_profile() -> Option<String> {
    let config = dirs::config_dir().unwrap();
    let profile_config_path = Path::new(&config).join("mapm").join("profile");
    if profile_config_path.is_file() {
        match fs::read_to_string(Path::new(&config).join("mapm").join("profile")) {
            Ok(profile_str) => return Some(profile_str),
            Err(err) => {
                e_red_ln!("{}", err);
                quit::with_code(exitcode::IOERR);
            }
        }
    }
    None
}

pub fn setup(profile: &str) {
    let config = dirs::config_dir().unwrap();
    fs::write(Path::new(&config).join("mapm").join("profile"), &profile).unwrap();
    let problem_path = Path::new(&config)
        .join("mapm")
        .join("problems")
        .join(&profile);
    if !problem_path.exists() {
        loop {
            green_ln!(
                "Would you like to make the directory `{}`? (y/n)",
                problem_path.to_str().unwrap()
            );
            let mut answer = String::new();
            io::stdin()
                .read_line(&mut answer)
                .expect("Could not read input line");
            answer = String::from(answer.trim());
            if &answer == "y" {
                fs::create_dir_all(
                    Path::new(&config)
                        .join("mapm")
                        .join("problems")
                        .join(&profile),
                )
                .expect("Could not create the directory.");
                break;
            } else if &answer == "n" {
                break;
            }
        }
    }
}

pub fn setup_prompt() {
    green_ln!(
        "You are setting the default profile for mapm right now. What would you like to call it?"
    );
    let mut profile = String::new();
    io::stdin()
        .read_line(&mut profile)
        .expect("Could not read input line");
    profile = String::from(profile.trim());
    if profile == "exit" || profile == "quit" {
    } else {
        setup(&profile);
    }
}
